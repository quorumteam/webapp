import Vue from 'vue'
import Vuex from 'vuex'
import { Message } from 'element-ui';
import { Notification } from 'element-ui';
import Chartist from 'chartist'
import Leaflet from 'leaflet'
import { analyseModule } from './modules/analyse-module.js'
import { geohashModule } from './modules/geohash-module.js'
import { autosendModule } from './modules/autosend-module.js'
import { formBuilderModule } from './modules/form-builder-module.js'
import { cartoModule } from './modules/carto-module.js'
import { navBarModule } from './modules/nav-bar-module.js'
import { settingsModule } from './modules/settings-module.js'
import { userModule } from './modules/user-module.js'
import { contactModule } from './modules/contact-module.js'

Vue.use(Vuex)

var communication_store = require('./../models/communication_store.js');
var contact_store = require('./../models/contact_store.js');
var user_store = require('./../models/user_store.js');
var session_store = require('./../models/session_store.js');
var forms_store = require('./../models/forms_store.js');
var ds_store = require('./../models/ds_store.js');
var insee_store = require('./../models/insee_store.js');

var methods = require('./../methods.js');
var charts = require('./../components/contact-module/contact-list/charts/generate.js');


const state = {
		authentificate : false,
		contacts : [],
		users : [],
		kpi : [],
		contactsCount : 0,
		usersCount : 0,
		filter : [],
		title : '',
		user : {},
		userConnected:{},
		activeUser : {},
		activeContact : {},
		query : '',
		active_request : false,
		sexe_filter : [],
		lastchange_filter : '',
		email_filter : [],
		age_filter : [],
		bdv_filter : [],
		errorMessage:'',
		sort_result: '',
		order:true,
		result_index : '0',
		sorting:'surname',
		total_hit: 0,
		pcount : 0,
		evolution : {},
		contacts_sans_email:0,
		contacts_sans_tel:0,
		percentage_HF : {'hommes' : 0, 'femmes' : 0, 'autres' : 0},
		is_loading : false,
		is_contact_loading : false,
		is_edited : false,
		forms : [],
		static : [],
		form_data: [],
		top_bdv : [],
		info_campagne : {},
		form_filter : false,
		form_filter_data : [],
		kpi_view : false,
		nb_results : 15,
		nb_results_users : 100,
		forms_presence : [],
		loadingExport : false,
		polygon: [],
		campagnes: [],
		// templates: [],
}

const mutations = {
	LOGOUT : state => {
		state.authentificate = false
		state.user  = {}
		state.userConnected  = {}
		state.sexe_filter = [];
		state.email_filter = [];
		state.lastchange_filter = '';
		state.age_filter = [];
		state.bdv_filter = [];
		state.query = '';
		state.sort_result = '';
		state.activeContact = {};
		state.activeUser = {};
		state.forms = [];
		state.static = [];
		state.forms_presence = [];
		state.contacts = [];
		state.users = [];
		state.kpi = [];
		state.filter = [];
		state.info_campagne = {};
		state.campagnes = [];
		// state.templates = [];
	},
	LOGIN : (state, user) =>  {
		state.authentificate = true ;
		state.userConnected = user;
	},
	SET_ACTIVE_CONTACT : (state, activeContact) => {
		state.activeContact = activeContact;
		if (activeContact.birthdate != undefined) {
			state.activeContact.age = methods.getAge(new Date(activeContact.birthdate))
			state.activeContact.birthdate_fr = methods.getDate(activeContact.birthdate)
		}

		if (activeContact.lastchange != undefined) state.activeContact.lastchange = methods.getDate(activeContact.lastchange)

	},
	DELETE_ACTIVE_CONTACT : state => {
		state.activeContact = {}
		state.form_data = {}
	},
	SET_ACTIVE_USER : (state, activeUser) => {
		console.log("state activeUser:")
		console.log(activeUser)
		state.activeUser = activeUser;
	},
	SAVE_ACTIVE_USER : (state, activeUser) => {
		for (let i = 0; i < state.users.length; i++) {
			if (activeUser.id == state.users[i].id) {
				// splice to trigger update
				state.users.splice(i, 1, activeUser);
				return;
			}
		}
		// if active user is a new user, push it onto the array
		state.users.push(activeUser);
	},
	DELETE_ACTIVE_USER : state => {
		state.activeUser = {}
	},

	SAVE_USER_CONNECTED : (state, userConnected) => {
		state.userConnected = userConnected;
		// update state.users if userConnected is in users
		for (let i = 0; i < state.users.length; i++) {
			if (userConnected.id == state.users[i].id) {
				// splice to trigger update
				state.users.splice(i, 1, userConnected);
				break;
			}
		}
	},

	SET_FILTERS_USERS : (state, obj) => {

		state.order = obj.order != undefined ? obj.order : state.order;
		state.result_index = obj.result_index != undefined ? obj.result_index : state.result_index;
		//state.filter = state.order;
	},

	//Initialisation des filtres
	SET_FILTERS : (state, obj) => {

		state.sexe_filter = obj.sexe_filter != undefined  ? obj.sexe_filter : state.sexe_filter;
		state.lastchange_filter = obj.lastchange_filter != undefined ? obj.lastchange_filter : state.lastchange_filter;
		state.email_filter = obj.email_filter != undefined ? obj.email_filter : state.email_filter;
		state.age_filter = obj.age_filter != undefined ? obj.age_filter : state.age_filter;
		state.bdv_filter = obj.bdv_filter != undefined ? obj.bdv_filter : state.bdv_filter;
		state.sorting= obj.sorting != undefined ? obj.sorting : state.sorting;
		state.query = obj.query != undefined ? obj.query : state.query;
		state.order = obj.order != undefined ? obj.order : state.order;
		state.result_index = obj.result_index != undefined ? obj.result_index : state.result_index;
		state.nb_results = obj.nb_results != undefined ? obj.nb_results : state.nb_results;
		state.nb_results_users = obj.nb_results_users != undefined ? obj.nb_results_users : state.nb_results_users;
		

		var tmp = new Array (
			state.userConnected.group_id.toString(),
			'all',
		  	state.nb_results.toString(),
		  	//state.nb_results_users.toString(),
		 	state.result_index.toString(),
		   	state.sexe_filter.toString(),
		    state.bdv_filter.toString(),
		    state.age_filter.toString(),
		    state.sorting != "" ? state.sorting : 'surname',
		    state.order.toString(),
			state.lastchange_filter,
			state.email_filter.toString(),
		);

		// Dans le cas du filtrage sur les forms, l'obj est un tableau de string contenant les filtres préformater
		// Voir recoverData() dans form-filters.vue pour plus de détails

		if ((Array.isArray(obj) && obj.length > 0) || state.form_filter_data.length > 0){

			if (!Array.isArray(obj)) {
				tmp = tmp.concat(state.form_filter_data)
			} else {
				tmp = tmp.concat(obj)
				state.form_filter_data = obj;
			}

			state.form_filter = true;
		};

		state.filter = tmp;

	},

	RESET_FILTERS : state => {
		state.sexe_filter = '';
		state.lastchange_filter = '';
		state.email_filter ='';
		state.age_filter = '';
		state.bdv_filter = '';
		state.sort_result= '';
		state.result_index = 0;
		state.query = '';
		state.order = true;
		state.polygon = []

		state.filter = [];
		state.contacts = [];
		state.contactsCount = 0;
		state.active_request = false;
		state.activeContact = {};
		state.form_filter_data = [];
		state.form_filter = false;
		state.nb_results = 15;
		state.nb_results_users = 100;

	},

	RESET_FORMS_FILTERS : state => {
		state.form_filter_data = [];
		state.form_filter = false;
	},

	SET_CONTACTS : (state, res) => {
		state.contacts = res;
		state.contactsCount = res.length;
		state.active_request = true;
	},

	SET_USERS : (state, res) => {
		state.users = res.users;
		state.usersCount = res.count;
	},

	SET_KPI : (state, res) => {
		state.kpi = res;
	},
	PAGE_COUNT : (state, res) => {
		state.pcount = res;
	},
	TOTAL_HIT : (state, res) => {
		state.total_hit = res;
	},
	SET_EVOLUTION : (state, res) => {
		state.evolution = res;
	},
	SET_CONTACTS_SANS_EMAIL : (state, res) => {
		state.contacts_sans_email = res;
	},
	SET_CONTACTS_SANS_TEL : (state, res) => {
		state.contacts_sans_tel = res;
	},

	SET_PERCENTAGE_HF :(state, res) => {
		state.percentage_HF.hommes = res.hommes != undefined  ? res.hommes : 0;
		state.percentage_HF.femmes = res.femmes != undefined  ? res.femmes : 0;
		state.percentage_HF.missing = res.missing != undefined  ? res.missing : 0;
	},
	LOADING: (state, res) => {
		state.is_loading = res;
	},
	CONTACT_LOADING: (state, res) => {
		state.is_contact_loading = res;
	},
	EDIT : (state, res) => {
		state.is_edited = res;
	},
	SET_FORMS : (state, res) => {
		state.forms = res;
	},
	SET_STATIC : (state, res) => {
		state.static = res;
	},
	SET_FORM_DATA : (state, res) => {
		state.form_data = res;
	},
	SET_TOP_BDV : (state, res) => {
		state.top_bdv = res;
	},
	SET_INFO_CAMPAGNE : (state, res) => {
		state.info_campagne = res;
	},
	SWITCH_KPI_VIEW : (state) => {
		state.kpi_view = !state.kpi_view;
	},
	SET_RESULT_INDEX : (state, res) => {
		state.result_index = res;
	},
	SET_FORMS_PRESENCE : (state, res) => {
		state.forms_presence = res;
	},
	SET_POLYGON : (state, res) => {
		state.polygon = res;
	},
	SET_CAMPAGNES : (state, res) => {
		state.campagnes = res;
	},
	SET_NB_RESULTS : (state, res) => {
		state.nb_results = res;
	},
	SET_NB_RESULTS_USERS : (state, res) => {
		state.nb_results_users = res;
	},
	
	// SET_TEMPLATES : (state, res) => {
	// 	state.templates = res;
	// }
}

const actions = {
	// Recherche sur la BDD
	SEARCH_CONTACT : (context, root) => {
		// if (state.query != "" || state.bdv_filter != "" || state.sexe_filter != "" || state.age_filter != "" || state.form_filter) {
			context.commit('DELETE_ACTIVE_CONTACT');
			context.commit('CONTACT_LOADING', true);

			contact_store.find(root, (res) => {
				context.commit('CONTACT_LOADING', false);
				context.commit('SET_CONTACTS', res);
	      	},context.state.query, context.state.filter, context.state.polygon)
		// };
	},
	UPDATE_LOCATION_SUMMARY_GEOHASH : (context, root) => {
		
	 	contact_store.locationSummaryGeoHash(root, (res) => {
	 		context.commit("SET_LOCATIONS_SUMMARY_GEOHASH", res.body.data.contacts);
	 		context.commit("SET_GEOHASH_ARRAY", res.body.data.data);
	 	},context.state.query, context.state.filter, context.state.polygon);

	},

	// Informations spécifiques d'un contact donné
	CONTACT_DETAIL : (context, contact) => {
		contact_store.first(null, contact.id, (res) => {
			context.commit('SET_ACTIVE_CONTACT', res);
		});
	},

	SEARCH_USER (context, root) {
		context.commit('DELETE_ACTIVE_USER')
		//console.log(state)
		user_store.list(root, (res) => {
			context.commit('SET_USERS', res);
		},state)
	},

	// Informations spécifiques d'un contact donné
	USER_DETAIL ({commit, state}, user) {
		user_store.first(null, user.id, (res) => {
			commit('SET_ACTIVE_USER', res);
		});
	},


	GENERATE_KPI : (context, root) => {
	  if (! context.state.active_request) {
	  	context.commit('SET_FILTERS', {})
	  };

	  contact_store.kpi(root, (res) => {
	  	if (res[0].KpiReplies != undefined && res[0].KpiReplies[0].Doc_count != 0) {

		  	context.commit('SET_KPI', res);
		  	context.commit('TOTAL_HIT', context.state.kpi[0].KpiReplies[0].Doc_count)

	        var pcount = Math.floor(context.state.kpi[0].KpiReplies[0].Doc_count / parseInt(context.state.nb_results));

	        if (context.state.total_hit % parseInt(context.state.nb_results) > 0) {
	          pcount+=1
	        }

	        context.commit('PAGE_COUNT', pcount);

	        

		  	var evolution = methods.generate_evolution(context.state.kpi[4].KpiReplies);
		  	var gender = methods.generate_gender(context.state.kpi[1]);
		  	var age = methods.generate_age(context.state.kpi[3].KpiReplies);
		  	var bdv = methods.generate_bdv(context.state.kpi[2].KpiReplies);
		  	var email = context.state.kpi[5].KpiReplies[0].Doc_count;;
		  	var tel = context.state.kpi[6].KpiReplies[0].Doc_count;;

		  	context.commit('SET_PERCENTAGE_HF', gender)
		  	context.commit('SET_EVOLUTION', evolution)
		  	context.commit('SET_CONTACTS_SANS_EMAIL', email)
		  	context.commit('SET_CONTACTS_SANS_TEL', tel)


		  	if (context.state.active_request) {
		  		charts.generate([[gender.femmes, gender.hommes, gender.missing], [bdv.label, bdv.bdv_data], age])
		  	} else {
		  		charts.generateKPI([[gender.femmes, gender.hommes, gender.missing], [bdv.label, bdv.bdv_data], age])
		  	}

		 	// console.log(bdv);
			// console.log(context);
			// console.log(res);
	  	};

	  }, context.state.query, context.state.filter, context.state.polygon)
	},

	// Récupérer les informations relatives à la campagne, la typologie des formulaies,  la priorisation et les contours des bureaux de voteS
	// (exécuté lors de la connexion)
	GLOBAL_DATA : (context, root) => {

		// Typologie des formulaires

		forms_store.get(root,  (res) => {
				context.commit('SET_FORMS', res)
		})

		forms_store.get_presence(null, (res) => {
			context.commit('SET_FORMS_PRESENCE', res)
		});

      	// Informations de campagne
      	session_store.mygroup((res) => {
       		context.commit('SET_INFO_CAMPAGNE', res)

        // 		Méthodes non fonctionnelles pour le moment (et non testées), voir explications dans ./models/ds_store.js
				//
				//   	ds_store.zones_vote(state.info_campagne.zone, (res) => {
	     // 		console.log(res)
				//   	});
				//
      	// 	ds_store.priorisation(state.info_campagne.parti, state.info_campagne.zone, state.info_campagne.echelle, (res) => {
      	// 		console.log(res)
      	// 	});
     	});

      	// TODO - these should probably be moved to their respective modules where the data is kept
     	forms_store.get_static(root, (res) => {
     		context.commit('SET_STATIC', res);
     	});

     	forms_store.get_campagnes(root, (res) => {
     		context.commit('SET_CAMPAGNES', res);
     	});
	}
}

const getters = {
	authentificate : state => {
		return state.authentificate;
	},
	activeContact : state => {
		return state.activeContact;
	},
	activeUser : state => {
		return state.activeUser;
	},
	userConnected : state => {
		return state.userConnected;
	},
	user : state => {
		return state.user;
	},
	query : state => {
		return state.query;
	},
	sexe_filter : state => {
		return state.sexe_filter;
	},
	lastchange_filter : state => {
		return state.lastchange_filter;
	},
	email_filter : state => {
		return state.email_filter;
	},
	age_filter: state => {
		return state.age_filter;
	},
	bdv_filter : state => {
		return state.bdv_filter;
	},
	bdv_label : state => {
		return state.bdv_label;
	},
	sort_result : state => {
		return state.sort_result;
	},
	order : state => {
		return state.order;
	},
	result_index : state => {
		return parseInt(state.result_index);
	},
	contactsCount : state => {
		return state.contactsCount;
	},
	usersCount : state => {
		return state.usersCount;
	},
	active_request : state => {
		return state.active_request;
	},
	contacts : state => {
		return state.contacts;
	},
	users : state => {
		return state.users;
	},
	kpi : state => {
		return state.kpi;
	},
	evolution : state => {
		return state.evolution;
	},
	contacts_sans_email : state => {
		return state.contacts_sans_email;
	},
	contacts_sans_tel : state => {
		return state.contacts_sans_tel;
	},
	total_hit : state => {
		return state.total_hit;
	},
	percentage_HF : state => {
		return state.percentage_HF;
	},
	pcount : state => {
		return state.pcount;
	},
	is_loading : state => {
		return state.is_loading;
	},
	is_contact_loading : state => {
		return state.is_contact_loading;
	},
	is_edited : state => {
		return state.is_edited;
	},
	forms : state => {
		return state.forms;
	},
	static : state => {
		return state.static;
	},
	forms_presence : state => {
		return state.forms_presence;
	},
	form_data : state => {
		return state.form_data;
	},
	filter : state => {
		return state.filter;
	},
	top_bdv : state => {
		return state.top_bdv;
	},
	info_campagne : state => {
		return state.info_campagne;
	},
	form_filter_data : state => {
		return state.form_filter_data;
	},
	form_filter : state => {
		return state.form_filter;
	},
	kpi_view : state => {
		return state.kpi_view;
	},
	nb_results : state => {
		return state.nb_results;
	},
	nb_results_users : state => {
		return state.nb_results_users;
	},
	loadingExport : state => {
		return state.loadingExport;
	},
	polygon : state => {
		return state.polygon
	},
	campagnes : state => {
		return state.campagnes
	},
	// templates : state => {
	// 	return state.templates
	// },
}

export default new Vuex.Store ({
	state ,
	mutations,
	getters,
	actions,

	modules: {
		analyse: analyseModule,
		geohash: geohashModule,
		autoSend: autosendModule,
		formBuilder: formBuilderModule,
		carto: cartoModule,
		navBar: navBarModule,
		settings: settingsModule,
		user: userModule,
		contact: contactModule,
	}
})
