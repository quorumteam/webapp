var contact_store = require('./../../models/contact_store.js');
//var state_store = require('./../stateStore.js');
const state = {
	location_summary_geohash : [],

	// map bounding box
	// northeast_lat : "",
	// northeast_lng : "",
	// southwest_lat : "",
	// southwest_lng : "",
	
	nb_resultats_location_summary_geohash : "500",
	precision_geohash : "5",
	geohash_array : [],
}

const mutations = {
  	SET_LOCATIONS_SUMMARY_GEOHASH : (state, res) => {
  		state.location_summary_geohash = res;
  	},
  	// SET_NORTHEAST_LAT : (state, res) => {
  	// 	state.northeast_lat = res;
  	// },
  	// SET_NORTHEAST_LNG : (state, res) => {
  	// 	state.northeast_lng = res;
  	// },
  	// SET_SOUTHWEST_LAT : (state, res) => {
  	// 	state.southwest_lat = res;
  	// },
  	// SET_SOUTHWEST_LNG : (state, res) => {
  	// 	state.southwest_lng = res;
  	// },
  	SET_NB_RESULTATS_LOCATION_SUMMARY_GEOHASH : (state, res) => {
  		state.nb_resultats_location_summary_geohash = res;
  	},
  	SET_PRECISION_GEOHASH : (state, res) => {
  		state.precision_geohash = res;
  	},
  	SET_GEOHASH_ARRAY : (state, res) => {
  		state.geohash_array = res;
  	},
  	
}

const actions = {

	RESET_BOUND_VARS : (context, root) => {
		context.commit("SET_NORTHEAST_LAT", "")
		context.commit("SET_NORTHEAST_LNG", "")
		context.commit("SET_SOUTHWEST_LAT", "")
		context.commit("SET_SOUTHWEST_LNG", "")
	},

	
}

const getters = {
  	location_summary_geohash : state => {
  		return state.location_summary_geohash
  	},
  	// northeast_lat : state => {
  	// 	return state.northeast_lat
  	// },
  	// northeast_lng : state => {
  	// 	return state.northeast_lng
  	// },
  	// southwest_lat : state => {
  	// 	return state.southwest_lat
  	// },
  	// southwest_lng : state => {
  	// 	return state.southwest_lng
  	// },
  	nb_resultats_location_summary_geohash : state => {
  		return state.nb_resultats_location_summary_geohash
  	},
  	precision_geohash : state => {
  		return state.precision_geohash
  	},
  	geohash_array : state => {
  		return state.geohash_array
  	}
}

export const geohashModule = {
  state : state,
  mutations : mutations,
  getters : getters,
  actions : actions,
}
