// module for the state store for cartographie
var insee_store = require('../../models/insee_store.js');

const state = {
    iris_data:[],
    iris_geo:[],
    zones_vote:[],
    code_commune:33063
  }

const mutations = {
  SET_IRIS_DATA : (state, res) => {
    state.iris_data = res;
  },
  SET_IRIS_GEO : (state, res) => {
    state.iris_geo = res;
  },
  SET_CODE_COMMUNE : (state, res) => {
    state.code_commune = res;
  },
  SET_ZONES_VOTE : (state, res) => {
    state.zones_vote = res;
  }
}

const actions = {
  RETRIEVE_IRIS_DATA : (context, root) =>{
    insee_store.find(root, (res) => {
      context.commit('SET_IRIS_DATA', res);
    },context.state.code_commune)
  },
  RETRIEVE_IRIS_GEO : (context, root) =>{
    insee_store.findGeo(root, (res) => {
      context.commit('SET_IRIS_GEO', res);
    },context.state.code_commune)
  },
  RETRIEVE_ZONES_VOTE : (context, root) =>{
    insee_store.zones_vote(root, (res) => {
      context.commit('SET_ZONES_VOTE', res);
    },context.state.code_commune)
  }
}

const getters = {
  iris_data : state => {
		return state.iris_data
	},
	iris_geo : state => {
		return state.iris_geo
	},
  zones_vote : state => {
		return state.zones_vote
	},
	code_commune : state => {
		return state.code_commune
	},
  get_iris_data : state => () => state.iris_data,
	get_iris_geo : state => () => state.iris_geo,
  get_zones_vote : state => () => state.zones_vote
}

// TODO - should initialize state.templates with API call

export const cartoModule = {
  state : state,
  mutations : mutations,
  getters : getters,
  actions : actions,
}
