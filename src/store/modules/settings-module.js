// module for the state store for form builder

const state = {
}

const mutations = {
}

const actions = {
}

const getters = {
}

export const settingsModule = {
  state : state,
  mutations : mutations,
  getters : getters,
  actions : actions,
}
