// module for the state store for auto-send emails
var communication_store = require('./../../models/communication_store.js');

const state = {
  autosend_email_acc_id : -1,
  autosend_sender : "",
  autosend_subject : "",
  autosend_header : "",
  autosend_footer : "",
  autosend_gender_label_homme : "",
  autosend_gender_label_femme : "",
  autosend_gender_label_neutre : "",
  emailaccs : [],
}

const mutations = {
  SET_AUTOSEND_EMAIL_ACC_ID : (state, res) => {
    state.autosend_email_acc_id = res;
  },
  SET_AUTOSEND_SENDER : (state, res) => {
    state.autosend_sender = res;
  },
  SET_AUTOSEND_SUBJECT : (state, res) => {
    state.autosend_subject = res;
  },
  SET_AUTOSEND_HEADER : (state, res) => {
    state.autosend_header = res;
  },
  SET_AUTOSEND_FOOTER : (state, res) => {
    state.autosend_footer = res;
  },
  SET_AUTOSEND_GENDER_LABEL_HOMME : (state, res) => {
    state.autosend_gender_label_homme = res;
  },
  SET_AUTOSEND_GENDER_LABEL_FEMME : (state, res) => {
    state.autosend_gender_label_femme = res;
  },
  SET_AUTOSEND_GENDER_LABEL_NEUTRE : (state, res) => {
    state.autosend_gender_label_neutre = res;
  },
  SET_EMAILACCS : (state, res) => {
    state.emailaccs = res;
  }
}

const actions = {
  // NOTE: this should be called whenever the values are needed on a new page; they are not guaranteed to be correct when loading a new page
  SETUP_AUTOSEND : (context, root) => {
    communication_store.get_autosend(root, (res) => {
      context.commit("SET_AUTOSEND_EMAIL_ACC_ID", res.emailacc_id);
      if (res.Emailacc) {
        context.commit("SET_AUTOSEND_SENDER", res.Emailacc.address);
      }
      context.commit("SET_AUTOSEND_SUBJECT", res.subject);
      context.commit("SET_AUTOSEND_HEADER", res.header);
      context.commit("SET_AUTOSEND_FOOTER", res.footer);
      context.commit("SET_AUTOSEND_GENDER_LABEL_HOMME", res.gender_label_homme);
      context.commit("SET_AUTOSEND_GENDER_LABEL_FEMME", res.gender_label_femme);
      context.commit("SET_AUTOSEND_GENDER_LABEL_NEUTRE", res.gender_label_neutre);
    })
  },

  SAVE_AUTOSEND : (context, root) => {
    var autosend = {
      emailacc_id : context.getters.autosend_email_acc_id,
      subject : context.getters.autosend_subject,
      header : context.getters.autosend_header,
      footer : context.getters.autosend_footer,
      gender_label_homme : context.getters.autosend_gender_label_homme,
      gender_label_femme : context.getters.autosend_gender_label_femme,
      gender_label_neutre : context.getters.autosend_gender_label_neutre,
    }
    console.log(autosend);
    // TODO - this function doesn't work... check backend
    communication_store.save_autosend(root, autosend, (res) => {
      console.log(res);
      // todo - commit result
    })
  },

  GET_EMAILACCS : (context, root) => {
    communication_store.get_emailaccs(root, (res) => {
      context.commit("SET_EMAILACCS", res);
    });
  },
}

const getters = {
  autosend_email_acc_id : state => {
    return state.autosend_email_acc_id
  },
  autosend_sender : state => {
    return state.autosend_sender
  },
  autosend_subject : state => {
    return state.autosend_subject
  },
  autosend_header : state => {
    return state.autosend_header
  },
  autosend_footer : state => {
    return state.autosend_footer
  },
  autosend_gender_label_homme : state => {
    return state.autosend_gender_label_homme
  },
  autosend_gender_label_femme : state => {
    return state.autosend_gender_label_femme
  },
  autosend_gender_label_neutre : state => {
    return state.autosend_gender_label_neutre
  },
  emailaccs : state => {
    return state.emailaccs
  },
}

// TODO - should initialize state.templates with API call

export const autosendModule = {
  state : state,
  mutations : mutations,
  getters : getters,
  actions : actions,
}
