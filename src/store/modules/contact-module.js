// module for the state store for users

const state = {
  // new_user is a boolean indicating if, when the contacts-list module is mounted, 
  // the new-contact sidebar should be visible
  new_contact: false,
}

const mutations = {
  SET_NEW_CONTACT : (state, res) => {
    state.new_contact = res;
  },

  RESET_NEW_CONTACT : (state) => {
    state.new_contact = false;
  }
}

const actions = {
}

const getters = {
  new_contact : state => {
    return state.new_contact
  },
}

// TODO - should initialize state.templates with API call

export const contactModule = {
  state : state,
  mutations : mutations,
  getters : getters,
  actions : actions,
}
