var contact_store = require('./../../models/contact_store.js');
var forms_store = require('./../../models/forms_store.js');

const state = {
	date_aggregation: [],
	aggregation : [],
	location_summary : [],

	total_count : 0,

	presence_form_id : -1,

	// aggregation_start/end are string dates in the form yyyy-mm-dd
	aggregation_start_filter : "",
	aggregation_end_filter : "",
	aggregation_start_ret : "",
	aggregation_end_ret : "",
	aggregation_interval : "",

	presence_filters : [],
	user_filters : [],

	// map bounding box
	northeast_lat : "",
	northeast_lng : "",
	southwest_lat : "",
	southwest_lng : "",
}

const mutations = {
  	SET_DATE_AGGREGATION : (state, res) => {
    	state.date_aggregation = res;
  	},
	SET_AGGREGATION : (state, res) => {
    	state.aggregation = res;
  	},
  	SET_LOCATIONS_SUMMARY : (state, res) => {
  		state.location_summary = res;
  	},
  	SET_TOTAL_COUNT : (state, res) => {
  		state.total_count = res;
  	},
  	SET_PRESENCE_FORM_ID : (state, res) => {
  		state.presence_form_id = res;
  	},
  	SET_AGGREGATION_START_FILTER : (state, res) => {
  		state.aggregation_start_filter = res;
  	},
  	SET_AGGREGATION_END_FILTER : (state, res) => {
  		state.aggregation_end_filter = res;
  	},
  	SET_AGGREGATION_START_RET : (state, res) => {
  		state.aggregation_start_ret = res;
  	},
  	SET_AGGREGATION_END_RET : (state, res) => {
  		state.aggregation_end_ret = res;
  	},
  	SET_AGGREGATION_INTERVAL : (state, res) => {
  		state.aggregation_interval = res;
  	},
  	SET_PRESENCE_FILTERS : (state, res) => {
  		state.presence_filters = res;
  	},
  	SET_USER_FILTERS : (state, res) => {
  		state.user_filters = res;
  	},
  	SET_NORTHEAST_LAT : (state, res) => {
  		state.northeast_lat = res;
  	},
  	SET_NORTHEAST_LNG : (state, res) => {
  		state.northeast_lng = res;
  	},
  	SET_SOUTHWEST_LAT : (state, res) => {
  		state.southwest_lat = res;
  	},
  	SET_SOUTHWEST_LNG : (state, res) => {
  		state.southwest_lng = res;
  	},
}

const actions = {
	GENERATE_AGGREGATIONS : (context, root) => {
		// reset the _start, _end and _interval
		context.dispatch("RESET_AGGREGATION_VARS")
		context.dispatch("RESET_BOUND_VARS")

		contact_store.dateAggregation(root, (res) => {
	 		context.commit("SET_DATE_AGGREGATION", res.body.data.kpi[0].KpiReplies);
	 	});

		// get the name_presence form id, then get the aggregation
		forms_store.get_presence(root, (res) => {
			for (var i = 0; i < res.length; i++) {
				if (res[i].name == "name_presence") {
					context.commit('SET_PRESENCE_FORM_ID', res[i].id);
					break;
				}
			}

			context.dispatch("UPDATE_AGGREGATION");
			context.dispatch("UPDATE_LOCATION_SUMMARY");
		})
		
	},

	RESET_AGGREGATION_VARS : (context, root) => {
		context.commit("SET_AGGREGATION_START_FILTER", "")
		context.commit("SET_AGGREGATION_END_FILTER", "")
		context.commit("SET_AGGREGATION_START_RET", "")
		context.commit("SET_AGGREGATION_END_RET", "")
		context.commit("SET_AGGREGATION_INTERVAL", "")
		context.commit("SET_PRESENCE_FILTERS", [])
		context.commit("SET_USER_FILTERS", [])
	},

	RESET_BOUND_VARS : (context, root) => {
		context.commit("SET_NORTHEAST_LAT", "")
		context.commit("SET_NORTHEAST_LNG", "")
		context.commit("SET_SOUTHWEST_LAT", "")
		context.commit("SET_SOUTHWEST_LNG", "")
	},

	UPDATE_AGGREGATION : (context, root) => {
		var fields = [
			context.getters.presence_form_id.toString(),
			context.getters.aggregation_start_filter, 
			context.getters.aggregation_end_filter,
			context.getters.northeast_lat,
			context.getters.northeast_lng,
			context.getters.southwest_lat,
			context.getters.southwest_lng
		];

		contact_store.aggregation(root, fields, (res) => {
	 		context.commit("SET_AGGREGATION", res.body.data.aggregation);
	 		var data = res.body.data.data;
	 		
	 		var commitKeys = {
	 			"minDate" : "SET_AGGREGATION_START_RET",
	 			"maxDate" : "SET_AGGREGATION_END_RET",
	 			"interval" : "SET_AGGREGATION_INTERVAL",
	 		}

	 		for (var key in commitKeys) {
	 			if (commitKeys.hasOwnProperty(key)) {
	 				for (var i = 0; i < data.length; i++) {
			 			if (data[i].Key == key) {
			 				context.commit(commitKeys[key], data[i].Value)
			 				break;
			 			}
			 		}
	 			}
	 		}
	 	});
	},

	UPDATE_LOCATION_SUMMARY : (context, root) => {
		var presenceFilter = "";
		var presence = context.getters.presence_filters.filter(function(filter) {
			return filter != null;
		});
		if (presence.length > 0) {
			presenceFilter = "presence;" + presence.join(";")
		}
		var usersFilter = "";
		var users = context.getters.user_filters.filter(function(filter) {
			return filter != null;
		});
		if (users.length > 0) {
			usersFilter = "user;" + users.join(";")
		}
		
		var fields = [
			context.getters.presence_form_id.toString(),
			context.getters.aggregation_start_filter, 
			context.getters.aggregation_end_filter,
			context.getters.northeast_lat,
			context.getters.northeast_lng,
			context.getters.southwest_lat,
			context.getters.southwest_lng,
			presenceFilter,
			usersFilter
		];

		contact_store.locationSummary(root, fields, (res) => {
	 		context.commit("SET_LOCATIONS_SUMMARY", res.body.data.contacts);
	 		var data = res.body.data.data;
	 		for (var i = 0; i < data.length; i++) {
	 			if (data[i].Key == "totalResults") {
	 				context.commit("SET_TOTAL_COUNT", parseInt(data[i].Value));
	 				break;
	 			}
	 		}
	 	});
	},
}

const getters = {
  	date_aggregation : state => {
  		return state.date_aggregation
  	},
	aggregation : state => {
    	return state.aggregation
  	},
  	location_summary : state => {
  		return state.location_summary
  	},
  	total_count : state => {
  		return state.total_count
  	},
  	presence_form_id : state => {
  		return state.presence_form_id
  	},
  	aggregation_start_filter : state => {
  		return state.aggregation_start_filter
  	},
  	aggregation_end_filter : state => {
  		return state.aggregation_end_filter
  	},
  	aggregation_start_ret : state => {
  		return state.aggregation_start_ret
  	},
  	aggregation_end_ret : state => {
  		return state.aggregation_end_ret
  	},
  	aggregation_interval : state => {
  		return state.aggregation_interval
  	},
  	presence_filters : state => {
  		return state.presence_filters
  	},
  	user_filters : state => {
  		return state.user_filters
  	},
  	northeast_lat : state => {
  		return state.northeast_lat
  	},
  	northeast_lng : state => {
  		return state.northeast_lng
  	},
  	southwest_lat : state => {
  		return state.southwest_lat
  	},
  	southwest_lng : state => {
  		return state.southwest_lng
  	},
}

export const analyseModule = {
  state : state,
  mutations : mutations,
  getters : getters,
  actions : actions,
}
