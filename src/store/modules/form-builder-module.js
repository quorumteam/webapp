// module for the state store for form builder

const state = {
  // sections represents the current, edited state of forms, organized by sections
  // it should default back to empty ([]) when the form is not currently being edited
  // also includes the templates for auto-send emails
  sections: []
}

const mutations = {
  SET_SECTIONS : (state, res) => {
    state.sections = res;
  },

  RESET_SECTIONS : (state) => {
    state.sections = [];
  },
}

const actions = {
}

const getters = {
  sections : state => {
    return state.sections
  },
}

// TODO - should initialize state.templates with API call

export const formBuilderModule = {
  state : state,
  mutations : mutations,
  getters : getters,
  actions : actions,
}
