// module for the state store for users

const state = {
  // menu_visible determines if the menu is visible when the screen is narrow
  menu_visible: false,
}

const mutations = {
  SET_MENU_VISIBLE : (state, res) => {
    state.menu_visible = res;
  },

  RESET_MENU_VISIBLE : (state) => {
    state.menu_visible = false;
  },

  TOGGLE_MENU_VISIBLE : (state) => {
  	state.menu_visible = !state.menu_visible;
  }
}

const actions = {
}

const getters = {
  menu_visible : state => {
    return state.menu_visible
  },
}

// TODO - should initialize state.templates with API call

export const navBarModule = {
  state : state,
  mutations : mutations,
  getters : getters,
  actions : actions,
}
