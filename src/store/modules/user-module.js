// module for the state store for users

const state = {
  // new_user is a boolean indicating if, when the user-list module is mounted, 
  // the new-user sidebar should be visible
  new_user: false,
}

const mutations = {
  SET_NEW_USER : (state, res) => {
    state.new_user = res;
  },

  RESET_NEW_USER : (state) => {
    state.new_user = false;
  }
}

const actions = {
}

const getters = {
  new_user : state => {
    return state.new_user
  },
}

// TODO - should initialize state.templates with API call

export const userModule = {
  state : state,
  mutations : mutations,
  getters : getters,
  actions : actions,
}
