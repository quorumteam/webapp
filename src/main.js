import Vue from 'vue'
import ElementUI from 'element-ui'
import Vuex from 'vuex'
import VueRouter from 'vue-router'
import store from './store/stateStore.js'
import router from './router.js'
import 'element-ui/lib/theme-default/index.css'
import locale from './fr.js'
import interceptor from './interceptor.js'

import VueVideoPlayer from 'vue-video-player'

// import Raven from 'raven-js';
// import RavenVue from 'raven-js/plugins/vue';

// Raven
//      .config('https://5c12cf3ea2164731a57476a4ce96c6f9@sentry.io/117124')
//      .addPlugin(RavenVue, Vue)
//      .install();

Vue.config.devtools =true;

Vue.use(ElementUI, {locale})
Vue.use(VueVideoPlayer)

var session_store = require('./models/session_store.js');
var _ = require('underskore');

router.beforeEach((to, from, next) => {

  var public_routes = ["login", "signup", "recover"];

  if (! _.contains(public_routes, to.name)) {
    var error = function() {
      next('login')
    }

    session_store.me((res) => {
      store.commit('LOGIN', res.body.data.user);
        (to.path === "/") ? next('home') : next()
    }, error);

  } else {

    if (_.contains(public_routes, to.name)) {
      session_store.delSession(this, (app) => {
        store.commit('LOGOUT');
      });
    };

    next();

  }

});



/* eslint-disable no-new */
const app = new Vue({
  el: '#app',
  router,
  store,
  render:h=>h(require('./App.vue'))
});
