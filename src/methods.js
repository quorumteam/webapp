import moment from 'moment'

var Emitter = require('events').EventEmitter,
    methods = module.exports = new Emitter();

methods.generate_evolution = function(data) {

    var number_of_change, personnes_rencontres, personnes_rencontres_last_week;

    if (data != null) {

        var lastchanges = [];

        for (var i = 0; i < data.length; i++) {
          lastchanges.push([data[i].Doc_count, moment(data[i].Key)]);
        };

        var personnes_rencontres = lastchanges[lastchanges.length-1][0];

        // Regarder si last change est dans la semaine courante 

        var current_week = moment().startOf('isoweek');

        var isInCurrentWeek = lastchanges[lastchanges.length-1][1].isSameOrAfter(current_week);

        if (!isInCurrentWeek) {
            personnes_rencontres = 0;
        }

        personnes_rencontres_last_week = lastchanges.length > 1 ? lastchanges[lastchanges.length-2][0] : 0;

        if (personnes_rencontres > personnes_rencontres_last_week && personnes_rencontres_last_week != 0 ) {
            number_of_change = Math.round(personnes_rencontres/personnes_rencontres_last_week);
        } else if(personnes_rencontres != 0 && personnes_rencontres_last_week != 0 && personnes_rencontres_last_week > personnes_rencontres) {
            number_of_change =  Math.round(personnes_rencontres_last_week/personnes_rencontres);
        } else if(personnes_rencontres != 0 && personnes_rencontres_last_week ==0){
            number_of_change = personnes_rencontres;
        }else{
            number_of_change = 0;
        }

    }else {
        number_of_change = 0;
        personnes_rencontres = 0;
        personnes_rencontres_last_week = 0;
    }

    return {
        'number_of_change' : number_of_change,
        'personnes_rencontres' : personnes_rencontres,
        'personnes_rencontres_last_week' : personnes_rencontres_last_week
    }
}

methods.generate_gender = function(data) {

    var hommes = 0, femmes = 0, na = 0;

      if (data.KpiReplies != null) {
        for (var i = 0; i < data.KpiReplies.length; i++) {

            if (data.KpiReplies[i].Key == "m" || data.KpiReplies[i].Key == "h") {
              hommes += data.KpiReplies[i].Doc_count;
            }
            if (data.KpiReplies[i].Key == "f") {
              femmes += data.KpiReplies[i].Doc_count;
            } 
            if(data.KpiReplies[i].Key == "missing"){
                na += data.KpiReplies[i].Doc_count;
            }
        };


      };
    // var obj = 

    // return an array 
    return  {'hommes' : hommes, 'femmes' : femmes, 'missing' : na}

}

methods.generate_age= function (data){
    var age_data = [];
      for (var i = 0; i < data.length; i++) {
          age_data.push(data[i].Doc_count)
      };
    return age_data;
}

methods.generate_bdv = function(data) {
      var label = [];
      var bdv_data = [];
      
      if (data != null) {
        var bdv_length = 0;
        data.length > 15 ? bdv_length = 15 : bdv_length =  data.length;

        for (var i = 0; i < bdv_length; i++) {
            label.push(data[i].Key)
            bdv_data.push(data[i].Doc_count)
        };

        return {'label' : label, 'bdv_data': bdv_data}
      };

}

methods.getAge = function(dateString) {
        var today = new Date();
        var birthDate = new Date(dateString);
        var age = today.getFullYear() - birthDate.getFullYear();
        var m = today.getMonth() - birthDate.getMonth();
        if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
            age--;
        }
        return age;
}
methods.getDate = function(birthdate) {
        var date =  new Date(birthdate)
        var jour = date.getDate();
        var mois = date.getMonth()+1;
        var an = date.getFullYear();

        return (jour + '/' + mois + '/' + an)
}
