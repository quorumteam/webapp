import { Message } from 'element-ui';
import router from './router.js';
import store from './store/stateStore.js';


(function(open) {

    XMLHttpRequest.prototype.open = function(method, url, async, userConnected, pass) {

        this.addEventListener("readystatechange", function() {

        	if (!navigator.onLine) {
				var freshMessage = 'Il semblerait que vous ne soyez pas connecté à internet';
				Message({
					message: freshMessage,
					showClose : true,
					type: 'warning'
				});
        	};

        	if (this.status != 0) {
        		if (this.status === 400) {

        			if (this.response != undefined) {
        				var response = JSON.parse(this.response);
        			};

					var freshMessage = 'Mauvais identifiants';

					Message({
						message: (response.message != undefined) ? response.message : freshMessage,
						duration:5000,
						type: 'error'
					});

				} else if (this.status >= 400 && this.status <= 403) {
					freshMessage="Problème d'authentification, veuillez vous re-authentifier"
					Message({
						message: freshMessage,
						duration:7000,
						type: 'error'
					});

					store.commit('LOGOUT');
					router.push({name: 'login'});

				} else if (this.status === 521 || this.status === 504 || this.status === 502) {

					freshMessage="Erreur serveur - veuillez réessayer et contacter votre support client si cela persiste"
					Message({
						message: freshMessage,
						duration:7000,
						type: 'error'
					});

					store.commit('LOGOUT');

					router.push({name: 'login'});


				}else if (this.status >=500 && this.status <600) {
					freshMessage="Erreur serveur - veuillez réessayer et contacter votre support client si cela persiste"
					Message({
						message: freshMessage,
						duration:7000,
						type: 'error'
					});
				} else if (this.status == 404) {
					freshMessage="Vous demandez une page qui n'existe pas"
					Message({
						message: freshMessage,
						duration:7000,
						type: 'error'
					});
				}

        	}
        }, false);

        open.call(this, method, url, async, userConnected, pass);
    };

})(XMLHttpRequest.prototype.open);
