import Vue from 'vue'
import VueRouter from 'vue-router'
import store from './store/stateStore.js'

Vue.use(VueRouter)

const routes = [{
  	path : '/login',
  	name : 'login',
    component : function(resolve) {
      require(['./components/app-module/Login.vue'], resolve);
    }
  },
  {
    path:'/recover',
    name : 'recover',
    component : function(resolve) {
      require(['./components/app-module/recover.vue'], resolve);
    }
  },
  {
    path:'/signup',
    name:'signup',
    component : function(resolve) {
      require(['./components/app-module/signup.vue'], resolve);
    }
  },
  {
  	path:'/',
  	name: 'dashboard',
  	component : function(resolve) {
      require(['./components/structure-module/Dashboard.vue'], resolve);
    },
  	children : [
      {
        path:'/home',
        name:'home',
        component : function(resolve) {
          require(['./components/home-module/home.vue'], resolve);
        }
      },
  		{
  			path:'/contacts',
        name:'contacts-list',
  			component : function(resolve) {
          require(['./components/contact-module/contact-list/contact-list.vue'], resolve);
        }
      },
      {
        path:'contacts/new',
        name:'new_contact',
        component : function(resolve) {
          require(['./components/contact-module/contact-details/contact-new.vue'], resolve);
        }
      },
			{
        path:'users/new',
        name:'new_user',
        component : function(resolve) {
          require(['./components/user_management-module/user-details/user-new.vue'], resolve);
        }
      },
			{
  			path:'/users',
        name:'users-list',
  			component : function(resolve) {
          require(['./components/user_management-module/user-list/user-list.vue'], resolve);
        }
      },
      {
        // Should be in french
        path:'parametre',
        name:'settings',
        component : function(resolve) {
          require(['./components/settings-module/settings.vue'], resolve);
        },
        children : [
          {
            path:'/parametre/campagne',
            name:'campaign',
            component : function(resolve) {
              require(['./components/settings-module/campaign-settings.vue'], resolve);
            }
          },
          {
            path:'/parametre/mailauto',
            name:'autosend-settings',
            component : function(resolve) {
              require(['./components/settings-module/autosend-settings.vue'], resolve);
            }
          }
        ]
      },
      {
        path:'/profil',
        name:'profile',
        component : function(resolve) {
          require(['./components/settings-module/profile.vue'], resolve);
        }
      },
			{
        path:'help',
        name:'help',
        component : function(resolve) {
          require(['./components/app-module/help.vue'], resolve);
        }
      },
      {
        path:'about',
        name:'about',
        component : function(resolve) {
          require(['./components/app-module/about.vue'], resolve);
        }
      },
      {
        path:'carte',
        name:'carte',
        component : function(resolve) {
          require(['./components/carto-module/carte.vue'], resolve);
        },children:[
            {
              path:'ciblage',
              name:'ciblage',
              component : function(resolve) {
                require(['./components/carto-module/ciblage.vue'], resolve);
              }
            },
            {
              path:'carto_insee',
              name:'carto_insee',
              component : function(resolve) {
                require(['./components/carto-module/carto_insee.vue'], resolve);
              }
            },
            {
              path:'carto',
              name:'carto',
              component : function(resolve) {
                require(['./components/carto-module/carto.vue'], resolve);
              }
            },
            {
              path:'test',
              name:'test',
              component : function(resolve) {
                require(['./components/carto-module/test.vue'], resolve);
              }
            },
            {
              path:'carto_insee_test_API',
              name:'carto_insee_test_API',
              component : function(resolve) {
                require(['./components/carto-module/carto_insee_test_API.vue'], resolve);
              }
            },
        ]
      },
      
      {
        path:'organisation',
        name:'organisation',
        component : function(resolve) {
          require(['./components/orga-module/organisation.vue'], resolve);
        }
      },
      {
        path:'invation-user',
        name:'invitation-user',
        component : function(resolve) {
          require(['./components/app-module/invitation-user.vue'], resolve);
        }
      },
      {
        path:'questionnaire',
        name:'form-builder',
        component : function(resolve) {
          require(['./components/form-module/form-builder.vue'], resolve);
        }
      },
      {
        path:'email',
        name:'email',
        component : function(resolve) {
          require(['./components/email-module/auto-send-container.vue'], resolve);
        }
      },
      {
        path:'guide',
        name:'guide',
        component : function(resolve) {
          require(['./components/app-module/guide.vue'], resolve);
        }
      },
      {
        path:'/analyse',
        name:'analyse',
        component : function(resolve) {
          require(['./components/analyse-module/analyse.vue'], resolve);
        },
        children : [
          {
            path:'/analyse/mobilisation',
            name:'mobilisation',
            component : function(resolve) {
              require(['./components/analyse-module/mobilisation.vue'], resolve);
            }
          },
          {
            path:'/analyse/equipe',
            name:'equipe',
            component : function(resolve) {
              require(['./components/analyse-module/equipe.vue'], resolve);
            }
          },
          {
            path:'/analyse/contacts',
            name:'contacts',
            component : function(resolve) {
              require(['./components/analyse-module/contacts.vue'], resolve);
            }
          },
          {
            path:'/analyse/enquete',
            name:'enquete',
            component : function(resolve) {
              require(['./components/analyse-module/enquete.vue'], resolve);
            }
          },
        ]
      },
  	]
  }];


export default  new VueRouter({
	mode: 'history',
	saveScrollPosition: true,
	routes: routes
});
