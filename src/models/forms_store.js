var Emitter = require('events').EventEmitter,
  forms = module.exports = new Emitter(),
  request = require('superagent'),
  nocache = require('superagent-no-cache'),
  common = require('./common.js'),
  api = common.api;

import stateStore from '../store/stateStore.js'


forms.get = function(root, cb) {
  request
  .get(`${api}/forms/name/FORM`)
  .use(nocache)
  .withCredentials()
  .set('Accept', 'application/json')
  .end(function(err, res) {
    if (err) {
      console.log('error')
    } else if (res.body.status === 'success') {
       cb(res.body.data.forms)
    } else if (common.token(res)) {
      // common.cb(root);
    } 
  });
};

// API endpoint not valid for single form
// forms.save = function(root, form, cb) {
//   request
//   .post(`${api}/forms`)
//   .use(nocache)
//   .withCredentials()
//   .set('Content-Type', 'application/json')
//   .send({
//     data: {
//       form: form
//     }
//   })
//   .end(function(err, res) {
//     if (err) {
//       console.log('error')
//     } else if (res.body.status === 'success') {
//       cb(res)
//     } else if (common.token(res)) {
//       common.cb(root);
//     } 
//   });
// };

forms.saveForms = function(root, forms, cb) {
  request
  .post(`${api}/forms`)
  .use(nocache)
  .withCredentials()
  .set('Content-Type', 'application/json')
  .send({
    data: {
      forms: forms
    }
  })
  .end(function(err, res) {
    if (err) {
      console.log('error')
    } else if (res.body.status === 'success') {
      cb(res)
    } else if (common.token(res)) {
      common.cb(root);
    } 
  });
};

forms.overwrite_forms = function(root, forms, cb) {
  request
  .patch(`${api}/forms`)
  .use(nocache)
  .withCredentials()
  .set('Content-Type', 'application/json')
  .send({
    data: {
      forms: forms
    }
  })
  .end(function(err, res) {
    if (err) {
      console.log('error')
    } else if (res.body.status === 'success') {
      cb(res)
    } else if (common.token(res)) {
      common.cb(root);
    } 
  });
};

// forms.overwrite_forms_templates = function(root, forms, templates, cb) {
//   request
//   .patch(`${api}/forms/templates`)
//   .use(nocache)
//   .withCredentials()
//   .set('Content-Type', 'application/json')
//   .send({
//     data: {
//       forms: forms,
//       templates: templates
//     }
//   })
//   .end(function(err, res) {
//     if (err) {
//       console.log('error')
//     } else if (res.body.status === 'success') {
//       cb(res)
//     } else if (common.token(res)) {
//       common.cb(root);
//     } 
//   });
// };

// forms.overwrite_forms_by_name_templates = function(root, forms, names, templates, cb) {
//   request
//   .patch(`${api}/forms/names/${names}/templates`)
//   .use(nocache)
//   .withCredentials()
//   .set('Content-Type', 'application/json')
//   .send({
//     data: {
//       forms: forms,
//       templates: templates
//     }
//   })
//   .end(function(err, res) {
//     if (err) {
//       console.log('error')
//     } else if (res.body.status === 'success') {
//       cb(res)
//     } else if (common.token(res)) {
//       common.cb(root);
//     } 
//   });
// };

// forms.overwrite_forms_by_name = function(root, forms, name, cb) {
//   request
//   .patch(`${api}/forms/name/${name}`)
//   .use(nocache)
//   .withCredentials()
//   .set('Content-Type', 'application/json')
//   .send({
//     data: {
//       forms: forms
//     }
//   })
//   .end(function(err, res) {
//     if (err) {
//       console.log('error')
//     } else if (res.body.status === 'success') {
//       cb(res)
//     } else if (common.token(res)) {
//       common.cb(root);
//     } 
//   });
// };

// this call can take a long time; it accepts an optional function that runs
// on completion (regardless of the success of the call)
forms.overwrite_forms_by_names = function(root, forms, names, cb, onReturn) {
  request
  .patch(`${api}/forms/names/${names}`)
  .use(nocache)
  .withCredentials()
  .set('Content-Type', 'application/json')
  .send({
    data: {
      forms: forms
    }
  })
  .end(function(err, res) {
    if (onReturn) {
      onReturn(res);
    }

    if (err) {
      console.log('error')
    } else if (res.body.status === 'success') {
      cb(res)
    } else if (common.token(res)) {
      common.cb(root);
    } 
  });
};

// store.save = function(root, contact, cb) {
//   request
//   .post(`${api}/contacts`)
//   .use(nocache)
//   .withCredentials()
//   .set('Content-Type', 'application/json')
//   .send({
//     data: {
//       contact: contact
//     }
//   })
//   .end(function(err, res) {
//     if (err) {
//       console.log('error')
//     } else if (res.body.status === 'success') {
//       cb(res);
//     } else if (common.token(res)) {
//       common.cb(root);
//     }
//   });
// };

forms.get_static = function(root, cb) {
  request
  .get(`${api}/forms/name/STATIC`)
  .use(nocache)
  .withCredentials()
  .set('Accept', 'application/json')
  .end(function(err, res) {
    if (err) {
      console.log('error')
    } else if (res.body.status === 'success') {
       cb(res.body.data.forms)
    } 
  });
};

forms.get_campagnes = function(root, cb) {
  request
  .get(`${api}/forms/name/campagne`)
  .use(nocache)
  .withCredentials()
  .set('Accept', 'application/json')
  .end(function(err, res) {
    if (err) {
      console.log('error')
    } else if (res.body.status === 'success') {
       cb(res.body.data.forms)
    }
  });
};

forms.get_presence = function(root, cb) {
  request
  .get(`${api}/forms/name/name_presence`)
  .use(nocache)
  .withCredentials()
  .set('Accept', 'application/json')
  .end(function(err, res) {
    if (err) {
      console.log('error')
    } else if (res.body.status === 'success') {
       cb(res.body.data.forms, root)
    } else if (common.token(res)) {
      common.cb(root);
    } 
  });
};
