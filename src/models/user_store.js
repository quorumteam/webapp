var Emitter = require('events').EventEmitter,
  store = module.exports = new Emitter(),
  request = require('superagent'),
  nocache = require('superagent-no-cache'),
  common = require('./common.js'),
  api = common.api;

  import stateStore from '../store/stateStore.js'

store.list = function(root, cb, state) {
  var limit;
  var offset;
  var sort;
  if (!state)
  {
    limit = 10000;
    offset = 0;
    sort = "true";
  }
  else{
    limit=state.nb_results_users;
    offset=state.result_index;
    sort=state.order
  }
  request
  .post(`${api}/users`)
  .use(nocache)
  .withCredentials()
  .set('Accept', 'application/json')
  .type('form')
  .send({
      limit: limit,
      offset: offset,
      sort: sort
  })
  .end(function(err, res) {
    if (err) {

    } else if (res.body.status === 'success') {
      // console.log(res)
      cb(res.body.data);
    } else if (common.token(res)) {
      common.cb(root);
    } else {
      stateStore.commit('ERROR','erreur dans store list');
      console.log("error");
    }
  });
};

store.find = function(root, cb, query, filter) {
  query="tou"
  filter= ["1", "all", "15", "0", "", "", "", "surname", "true"]
  request
  .post(`${api}/search`)
  .use(nocache)
  .withCredentials()
  .set('Accept', 'application/json')
  .send({
    data: {
      query: query,
      fields: filter
  }
  })
  .end(function(err, res) {
    if (err) {

    } else if (res.body.status === 'success') {
      cb(res.body.data.users);
    } else if (common.token(res)) {
      common.cb(root);
    } else {
      console.log("error");
    }
  });
};



store.first = function(root, id, cb) {
  request
  .get(`${api}/users/${id}`)
  .use(nocache)
  .withCredentials()
  .set('Accept', 'application/json')
  .end(function(err, res) {
    if (err) {
      console.log('error')
    } else if (res.body.status === 'success') {
      cb(res.body.data.user);
    } else if (common.token(res)) {
      common.cb(root);
    } else {
      console.log("error");
    }
  });
};
/*
storeEmit.signup = function(data, cb, cbError, root) {
  request
  .post(`${api}/user`)
  .use(nocache)
  .set('Accept', 'application/json')
  .type('form')
  .send(data)
  .end(function(err, res) {
    if (err) {
      stateStore.commit('ERROR',res);
    } else if (res.status === 200) {
      cb(root);
    }
  });
}

session_store.signup(this.userForm, cb, cbError, this.$root);
*/
store.save = function(root, user, cb) {
  request
  .post(`${api}/usernopassword`)
  .use(nocache)
  .withCredentials()
  .set('Content-Type', 'application/json')
  .type('form')
  .send(user)
  .end(function(err, res) {
    if (err) {

    } else if (res.body.status === 'success') {
      cb(res);
    } else if (common.token(res)) {
      common.cb(root);
    } else {
      console.log("error");
    }
  });
};

store.update = function(root, user, cb) {
console.log("user");
  console.log(user);
  request
  .patch(`${api}/users/${user.id}`)
  .use(nocache)
  .withCredentials()
  .set('Content-Type', 'application/json')
  .type('form')
  .send(user)
  .end(function(err, res) {
    if (err) {

    } else if (res.body.status === 'success') {
      cb(res);
    } else if (common.token(res)) {
      common.cb(root);
    } else {
      console.log("error");
    }
  });
};

store.delete = function(root, id, cb) {
  request
  .del(`${api}/users/${id}`)
  .use(nocache)
  .withCredentials()
  .end(function(err, res) {
    if (err) {

    }
    if (!err) {
      cb(res);
    } else if (common.token(res)) {
      common.cb(root);
    } else {
      console.log("error");
    }
  });
};
