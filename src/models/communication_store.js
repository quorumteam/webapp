var Emitter = require('events').EventEmitter,
  communication = module.exports = new Emitter(),
  request = require('superagent'),
  nocache = require('superagent-no-cache'),
  common = require('./common.js'),
  api = common.api;

import stateStore from '../store/stateStore.js'

communication.get_templates = function(root, cb) {
  request
  .get(`${api}/templates`)
  .use(nocache)
  .withCredentials()
  .set('Accept', 'application/json')
  .end(function(err, res) {
    if (err) {
      console.log('error')
    } else if (res.body.status === 'success') {
       cb(res.body.data.templates)
    } else if (common.token(res)) {
      // common.cb(root);
    } 
  });
};

communication.save_template = function(root, template, cb) {
  request
  .post(`${api}/templates`)
  .use(nocache)
  .withCredentials()
  .set('Content-Type', 'application/json')
  .send({
    data: {
      template: template
    }
  })
  .end(function(err, res) {
    if (err) {
      console.log('error')
    } else if (res.body.status === 'success') {
      cb(res)
    } else if (common.token(res)) {
      common.cb(root);
    } 
  });
};

communication.delete_template = function(root, id, cb) {
  request
  .delete(`${api}/templates/${id}`)
  .use(nocache)
  .withCredentials()
  .set('Accept', 'application/json')
  .end(function(err, res) {
    console.log(res);
    if (err) {
      console.log('error')
    } else if (res.status == 204 || res.body.status === 'success') {
      // for a delete, a 204 (empty response) is also a valid, successful return
      cb(res)
    } else if (common.token(res)) {
      common.cb(root);
    } 
  });
};

communication.get_autosend = function(root, cb) {
  request
  .get(`${api}/autosends`)
  .use(nocache)
  .withCredentials()
  .set('Accept', 'application/json')
  .end(function(err, res) {
    if (err) {
      console.log('error')
    } else if (res.body.status === 'success') {
       cb(res.body.data.autosend)
    } else if (common.token(res)) {
      // common.cb(root);
    } 
  });
};

communication.save_autosend = function(root, autosend, cb) {
  request
  .post(`${api}/autosends`)
  .use(nocache)
  .withCredentials()
  .set('Accept', 'application/json')
  .send({
    data: {
      autosend: autosend
    }
  })
  .end(function(err, res) {
    if (err) {
      console.log('error')
    } else if (res.body.status === 'success') {
       cb(res.body.data.autosend)
    } else if (common.token(res)) {
      // common.cb(root);
    } 
  });
};

communication.get_emailaccs = function(root, cb) {
  request
  .get(`${api}/emailaccs`)
  .use(nocache)
  .withCredentials()
  .set('Accept', 'application/json')
  .end(function(err, res) {
    if (err) {
      console.log('error')
    } else if (res.body.status === 'success') {
       cb(res.body.data.emailaccs)
    } else if (common.token(res)) {
      // common.cb(root);
    } 
  });
};

communication.send_email = function(root, data, cb) {
  request
  .post(`${api}/sendemail`)
  .use(nocache)
  .withCredentials()
  .set('Accept', 'application/json')
  .send({
    data: data
  })
  .end(function(err, res) {
    if (err) {
      console.log('error')
    } else if (res.body.status === 'success') {
       cb(res.body.data)
    } else if (common.token(res)) {
      // common.cb(root);
    } 
  });
};
