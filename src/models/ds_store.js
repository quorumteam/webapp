var Emitter = require('events').EventEmitter,
  ds = module.exports = new Emitter(),
  request = require('superagent'),
  nocache = require('superagent-no-cache'),
  common = require('./common.js'),
  api = common.api_priorisation;

// /!\ Non fonctionnel à cause du protocole  CORS :
//  	- Contournable sur express | nodejs (pour zones_vote)
// 		- Non contournable sur plumber (https://github.com/trestletech/plumber/issues/66)

// Pas de workaround à court terme, il faut ajouter les APIs dans la gateway

ds.priorisation = function(parti, zone, echelle, cb) {
  request
  .get(`${api}:8058/UseAlgo?`)
  .query({Parti : parti, Echelle : echelle, Zone : zone})
  .use(nocache)
  .set('Accept', 'application/json')
  .end(function(err, res) {
    if (err) {
      // stateStore.commit('ERROR',err);
    } else if (res.body.status === 'success') {
       cb(res)
    } else {
      // stateStore.commit('ERROR','erreur dans ds.priorisation');
      console.log("error");
    }
  });
};

ds.zones_vote = function(zone, cb) {
  request
  .get(`${api}:8052/`)
  .query({code_commune : zone})
  .use(nocache)
  .withCredentials()
  .set('Accept', 'application/json')
  .end(function(err, res) {
    if (err) {
      // stateStore.commit('ERROR',err);
      console.log(err)
    } else if (res.body.status === 'success') {
       cb(res)
    } else {
      // stateStore.commit('ERROR','erreur dans ds.priorisation');
      console.log("error");
    }
  });
};
