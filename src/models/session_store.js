var Emitter = require('events').EventEmitter,
  storeEmit = module.exports = new Emitter(),
  request = require('superagent'),
  nocache = require('superagent-no-cache'),
  common = require('./common.js'),
  api = common.api;

import stateStore from '../store/stateStore.js'


storeEmit.me = function(cb, cbError) {
  request
  .get(`${api}/me`)
  .use(nocache)
  .withCredentials()
  .set('Accept', 'application/json')
  .end(function(err, res) {
     if (res.body.status === 'success') {
      cb(res);
    } else {
      cbError(res)
    }
  });
}

storeEmit.mygroup = function(cb, cbError, root) {
  request
  .get(`${api}/mygroup`)
  .use(nocache)
  .withCredentials()
  .set('Accept', 'application/json')
  .end(function(err, res) {
    if (common.token(res)) {
       cb(root);
    } else if (res.body.status === 'success') {
      cb(res.body.data.group);
    }
  });
}


storeEmit.getSession = function(data, cb, cbError) {
  request
  .post(`${api}/session`)
  .use(nocache)
  .withCredentials()
  .set('Accept', 'application/json')
  .type('form')
  .send(data)
  .end(function(err, res) {
    if (err)
    {

    }
    else if (res.body.status === 'success') {
      cb();
    }
  });
}

storeEmit.delSession = function(app, cb) {
  request
  .del(`${api}/session`)
  .use(nocache)
  .withCredentials()
  .set('Accept', 'application/json')
  .end(function(err, res) {
      cb(app);
  });
}

storeEmit.signup = function(data, cb, cbError, root) {
  request
  .post(`${api}/user`)
  .use(nocache)
  .set('Accept', 'application/json')
  .type('form')
  .send(data)
  .end(function(err, res) {
    if (res.status === 200) {
      cb(root);
    }
  });
}


storeEmit.password = function(data, cb, cbError, root) {
  request
  .post(`${api}/password`)
  .use(nocache)
  .set('Accept', 'application/json')
  .type('form')
  .send(data)
  .end(function(err, res) {
    if (res.status === 200) {
      cb(root);
    }
  });
}
