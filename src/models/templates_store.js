var Emitter = require('events').EventEmitter,
  templates = module.exports = new Emitter(),
  request = require('superagent'),
  nocache = require('superagent-no-cache'),
  common = require('./common.js'),
  api = common.api;

import stateStore from '../store/stateStore.js'

// TODO - this isn't actually implemented on the backend yet :(
templates.get = function(root, cb) {
  request
  .get(`${api}/templates`)
  .use(nocache)
  .withCredentials()
  .set('Accept', 'application/json')
  .end(function(err, res) {
    if (err) {
      console.log('error')
    } else if (res.body.status === 'success') {
       cb(res.body.data.forms)
    }
  });
};