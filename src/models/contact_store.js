var Emitter = require('events').EventEmitter,
  store = module.exports = new Emitter(),
  request = require('superagent'),
  nocache = require('superagent-no-cache'),
  common = require('./common.js'),
  api = common.api;

  import stateStore from '../store/stateStore.js'

store.list = function(root, cb) {
  request
  .get(`${api}/contacts`)
  .use(nocache)
  .withCredentials()
  .set('Accept', 'application/json')
  .end(function(err, res) {
    if (err) {
      // stateStore.commit('ERROR',err);
    } else if (res.body.status === 'success') {
      // console.log(res)
      cb(res.body.data.contacts);
    } else if (common.token(res)) {
      common.cb(root);
    } else {
      console.log("error");
    }
  });
};

store.find = function(root, cb, query, filter, polygon) {
  request
  .post(`${api}/search`)
  .use(nocache)
  .withCredentials()
  .set('Accept', 'application/json')
  .send({
    data: {
      query: query,
      fields: filter,
      polygon : polygon
  }
  })
  .end(function(err, res) {
    if (err) {
      // stateStore.commit('ERROR',err);
    } else if (res.body.status === 'success') {
      if (res.body.data.contacts != undefined) cb(res.body.data.contacts);
      if (res.body.data.AddressAggs != undefined) cb(res.body.data.AddressAggs);
    } else if (common.token(res)) {
      common.cb(root);
    } else {
      console.log("error");
    }
  });
};


store.kpi = function(root, cb, query, filter, polygon) {
 request
 .post(`${api}/kpi`)
 .use(nocache)
 .withCredentials()
 .set('Accept', 'application/json')
 .send({
   data: {
     query: query,
     fields: filter,
     polygon : polygon
 }
 })
 .end(function(err, res) {
   if (err) {
     // stateStore.commit('ERROR',err);
   }
   if (res.body.status === 'success') {
     // console.log(res.body.data.kpi)
     cb(res.body.data.kpi);
   } else if (common.token(res)) {
     common.cb(root);
   } else {
     console.log("error");
   }
 });
};

store.aggregation = function(root, fields, cb) {
  request
  .post(`${api}/aggregation`)
  .use(nocache)
  .withCredentials()
  .set('Accept', 'application/json')
  .send({
    data: {
      fields: fields
    }
  })
  .end(function(err, res) {
   if (err) {
     // stateStore.commit('ERROR',err);
   }
   if (res.body.status === 'success') {
     // console.log(res.body.data.kpi)
     // cb(res.body.data.aggregation);
     cb(res);
   } else if (common.token(res)) {
     common.cb(root);
   } else {
     console.log("error");
   }
  });
}

store.dateAggregation = function(root, cb) {
  request
  .post(`${api}/dateAggregation`)
  .use(nocache)
  .withCredentials()
  .set('Accept', 'application/json')
  .send({})
  .end(function(err, res) {
   if (err) {
     // stateStore.commit('ERROR',err);
   }
   if (res.body.status === 'success') {
     // console.log(res.body.data.kpi)
     // cb(res.body.data.aggregation);
     cb(res);
   } else if (common.token(res)) {
     common.cb(root);
   } else {
     console.log("error");
   }
  });
}

store.locationSummary = function(root, fields, cb) {
  request
  .post(`${api}/locationSummary`)
  .use(nocache)
  .withCredentials()
  .set('Accept', 'application/json')
  .send({
    data: {
      fields: fields
    }
  })
  .end(function(err, res) {
   if (err) {
     // stateStore.commit('ERROR',err);
   }
   if (res.body.status === 'success') {
     // console.log(res.body.data.kpi)
     // cb(res.body.data.aggregation);
     cb(res);
   } else if (common.token(res)) {
     common.cb(root);
   } else {
     console.log("error");
   }
  });
}

store.locationSummaryGeoHash = function(root, cb, query, filter, polygon) {
  request
  .post(`${api}/locationSummaryContactsGeoHash`)
  .use(nocache)
  .withCredentials()
  .set('Accept', 'application/json')
  .send({
   data: {
     query: query,
     fields: filter,
     polygon : polygon
  }
 })
  .end(function(err, res) {
   if (err) {
     // stateStore.commit('ERROR',err);
   }
   if (res.body.status === 'success') {
     // console.log(res.body.data.kpi)
     // cb(res.body.data.aggregation);
     cb(res);
   } else if (common.token(res)) {
     common.cb(root);
   } else {
     console.log("error");
   }
  });
}

store.first = function(root, id, cb) {
  request
  .get(`${api}/contacts/${id}`)
  .use(nocache)
  .withCredentials()
  .set('Accept', 'application/json')
  .end(function(err, res) {
    if (err) {
      // stateStore.commit('ERROR',err);
    } else if (res.body.status === 'success') {
      cb(res.body.data.contact);
    } else if (common.token(res)) {
      common.cb(root);
    } else {
      console.log("error");
    }
  });
};

// this call can take a bit of time; so it accepts an optional function that runs
// on completion (regardless of the success of the call)
store.save = function(root, contact, cb, onReturn) {
  request
  .post(`${api}/contacts`)
  .use(nocache)
  .withCredentials()
  .set('Content-Type', 'application/json')
  .send({
    data: {
      contact: contact
    }
  })
  .end(function(err, res) {
    if (onReturn) {
      onReturn(res);
    }

    if (err) {
      console.log('error')
    } else if (res.body.status === 'success') {
      cb(res);
    } else if (common.token(res)) {
      common.cb(root);
    }
  });
};

store.update = function(root, contact, cb) {
  request
  .patch(`${api}/contacts/${contact.id}`)
  .use(nocache)
  .withCredentials()
  .set('Content-Type', 'application/json')
  .send({
    data: {
      contact: contact
    }
  })
  .end(function(err, res) {
    if (err) {
      console.log('error')
    } else if (res.body.status === 'success') {
      cb(res);
    } else if (common.token(res)) {
      common.cb(root);
    } 
  });
};

store.delete = function(root, id, cb) {
  request
  .del(`${api}/contacts/${id}`)
  .use(nocache)
  .withCredentials()
  .end(function(err, res) {
    if (err) {
      console.log('error')
    }
    if (!err) {
      cb(res);
    } else if (common.token(res)) {
      common.cb(root);
    } 
  });
};
