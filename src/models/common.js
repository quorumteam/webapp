var Emitter = require('events').EventEmitter,
    common = module.exports = new Emitter(),
    data = "Token",
   	// main = require('../components/app-module/index.js'),
	status = "fail";



// common.api = 'http://192.168.20.117:8080';
//common.api = 'https://integration.quorumapps.com';
common.api = 'https://api.quorumapps.com';
//common.api = 'http://localhost:8080';
common.api_priorisation = 'http://138.68.94.27';



common.token = function(res) {

	if (res != undefined ) {
		return (res.body.data == data && res.body.status == status) ? 1 : 0 ;
	}

	return 1;

}

common.cb = function(root) { root.push('{name : login}')}
