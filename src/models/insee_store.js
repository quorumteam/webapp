var Emitter = require('events').EventEmitter,
  insee = module.exports = new Emitter(),
  request = require('superagent'),
  nocache = require('superagent-no-cache'),
  common = require('./common.js'),
  api = common.api;
  //api_priorisation = common.api_priorisation;

  import stateStore from '../store/stateStore.js'

// store.list = function(root, cb) {
//   request
//   .get(`${api}/contacts`)
//   .use(nocache)
//   .withCredentials()
//   .set('Accept', 'application/json')
//   .end(function(err, res) {
//     if (err) {
//       // stateStore.commit('ERROR',err);
//     } else if (res.body.status === 'success') {
//       // console.log(res)
//       cb(res.body.data.contacts);
//     } else if (common.token(res)) {
//       common.cb(root);
//     } else {
//       console.log("error");
//     }
//   });
// };

insee.find = function(root, cb, code_commune) {
  request
  .get(`http://138.68.70.201:8051/Traitement?Code_commune=`+code_commune)
  .set('Accept', 'application/json')
  .end(function(err, res) {
    console.log(res)
    if (err) {
      // stateStore.commit('ERROR',err);
    } else if (res.status == 200) {
      if (res.body != undefined) cb(res.body);
    } else if (common.token(res)) {
      common.cb(root);
    } else {
      console.log("error find");
    }
  });
};

insee.findGeo = function(root, cb, code_commune) {
  request
  .get(`http://138.68.70.201:8090/?Code_Commune=`+code_commune)
  .set('Accept', 'application/json')
  .end(function(err, res) {
    console.log(res)
    if (err) {
      // stateStore.commit('ERROR',err);
    } else if (res.status == 200) {
      if (res.body != undefined) cb(res.body);
    } else if (common.token(res)) {
      common.cb(root);
    } else {
      console.log("error findGeo");
    }
  });
};

insee.zones_vote = function(root, cb, code_commune) {
  request
  .get(`http://138.68.70.201:8052/?code_commune=`+code_commune)
  .set('Accept', 'application/json')
  .end(function(err, res) {
    console.log(res)
    if (err) {
      // stateStore.commit('ERROR',err);
    } else if (res.status == 200) {
      if (res.body != undefined) cb(res.body);
    } else if (common.token(res)) {
      common.cb(root);
    } else {
      console.log("error zones_vote");
    }
  });
};


// store.kpi = function(root, cb, query, filter, polygon) {
//  request
//  .post(`${api}/kpi`)
//  .use(nocache)
//  .withCredentials()
//  .set('Accept', 'application/json')
//  .send({
//    data: {
//      query: query,
//      fields: filter,
//      polygon : polygon
//  }
//  })
//  .end(function(err, res) {
//    if (err) {
//      // stateStore.commit('ERROR',err);
//    }
//    if (res.body.status === 'success') {
//      // console.log(res.body.data.kpi)
//      cb(res.body.data.kpi);
//    } else if (common.token(res)) {
//      common.cb(root);
//    } else {
//      console.log("error");
//    }
//  });
// };
//
// store.first = function(root, id, cb) {
//   request
//   .get(`${api}/contacts/${id}`)
//   .use(nocache)
//   .withCredentials()
//   .set('Accept', 'application/json')
//   .end(function(err, res) {
//     if (err) {
//       // stateStore.commit('ERROR',err);
//     } else if (res.body.status === 'success') {
//       cb(res.body.data.contact);
//     } else if (common.token(res)) {
//       common.cb(root);
//     } else {
//       console.log("error");
//     }
//   });
// };
//
// store.save = function(root, contact, cb) {
//   request
//   .post(`${api}/contacts`)
//   .use(nocache)
//   .withCredentials()
//   .set('Content-Type', 'application/json')
//   .send({
//     data: {
//       contact: contact
//     }
//   })
//   .end(function(err, res) {
//     if (err) {
//       console.log('error')
//     } else if (res.body.status === 'success') {
//       cb(res);
//     } else if (common.token(res)) {
//       common.cb(root);
//     }
//   });
// };
//
// store.update = function(root, contact, cb) {
//   request
//   .patch(`${api}/contacts/${contact.id}`)
//   .use(nocache)
//   .withCredentials()
//   .set('Content-Type', 'application/json')
//   .send({
//     data: {
//       contact: contact
//     }
//   })
//   .end(function(err, res) {
//     if (err) {
//       console.log('error')
//     } else if (res.body.status === 'success') {
//       cb(res);
//     } else if (common.token(res)) {
//       common.cb(root);
//     }
//   });
// };
//
// store.delete = function(root, id, cb) {
//   request
//   .del(`${api}/contacts/${id}`)
//   .use(nocache)
//   .withCredentials()
//   .end(function(err, res) {
//     if (err) {
//       console.log('error')
//     }
//     if (!err) {
//       cb(res);
//     } else if (common.token(res)) {
//       common.cb(root);
//     }
//   });
// };
