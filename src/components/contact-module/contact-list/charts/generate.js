var Emitter = require('events').EventEmitter,
    charts = module.exports = new Emitter(),
    _ = require('underskore');


var $ = require("jquery");

import Chartist from 'chartist'
import Chart from 'chart.js'


charts.generate = function(input_data) {
  setTimeout(function(){

      var gender = []
      var gender_type = ['femmes', 'hommes', 'missing-na' ]
      var sum_tot = _.reduce(input_data[0], function(memo, num){ return memo + num; }, 0);


      input_data[0].forEach(function (n,index) {
        if (Math.round((n/sum_tot)*1000)/10 > 0) {
          gender.push({'value': n, 'className': gender_type[index]})
        };
      });

      var data = {
        series: gender
      };

      var options = {
        width: 100,
        height: 150
      };


      if (document.getElementsByClassName('ct-chart').length > 0) {
        new Chartist.Pie('.ct-chart', data, options)
      };

      var sum = _.reduce(input_data[2], function(memo, num){ return memo + num; }, 0);
      var percentage_pop = []

      var age_labels =  ['N/A','- 18', '18-25', '25-35', '35-50', '50-65', '+ 65'];

      for (var i = 0; i < input_data[2].length; i++) {
        age_labels[i] += '<br/> (' + (Math.round(((input_data[2][i]/sum)*100)*100)/100).toString() + " %)";
      };

      var data = {
        labels: age_labels,
        series: [
          input_data[2]
        ]
      };

      if (document.getElementsByClassName('age-chart').length > 0) {
        new Chartist.Bar('.age-chart', data).on("draw", function(bars) {
          bars.element.animate({
            y2: {
              begin: 0,
              dur: 500,
              from: bars.y1,
              to: bars.y2
            }
          })
        });
      }

      var data = {
        labels: input_data[1][0],
        series: [
          input_data[1][1]
        ]
      };

      if (document.getElementsByClassName('bdv-chart').length > 0) {
        new Chartist.Bar('.bdv-chart', data).on("draw", function(bars) {
        bars.element.animate({
          y2: {
            begin: 0,
            dur: 500,
            from: bars.y1,
            to: bars.y2
          }
        })
      });
      }


      var modified_labels = [];

      for (var i = 0; i < input_data[1][0].length; i++) {
        modified_labels.push({meta : input_data[1][0][i].toString(), value : input_data[1][1][i]})
      };

      var data = {
        series: [modified_labels]
      };

      if (document.getElementsByClassName('bdv-chart').length > 0) {

        var chart = new Chartist.Bar('.bdv-chart', data).on("draw", function(bars) {
                    bars.element.animate({
                      y2: {
                        begin: 0,
                        dur: 500,
                        from: bars.y1,
                        to: bars.y2
                      }
                    })
                  });

        var addedEvents = false;

        chart.on('draw', function() {
          if (!addedEvents) {

            $('.ct-bar').on('mouseover', function() {
              if ($(this).attr('ct:meta') != undefined) {
                $('.legende-bdv')[0].innerHTML = $(this).attr('ct:meta');
              }
            });

            $('.ct-bar').on('mouseout', function() {
              if ($(this).attr('ct:meta') != undefined) {
                $('.legende-bdv')[0].innerHTML = '';
              }
            });

          }
        });
      }



    },10);
}

charts.generateKPI = function(input_data) {



    var gender = []
    var gender_type = ['femmes', 'hommes', 'missing-na' ]
    var sum_tot = _.reduce(input_data[0], function(memo, num){ return memo + num; }, 0);

    input_data[0].forEach(function (n,index) {
      if (Math.round((n/sum_tot)*1000)/10 > 0) {
        gender.push({'value': n, 'className': gender_type[index]})
      };
    });


    var data = {
      series: gender
    };


    var options = {
      width: 150,
      height: 150
    };

    var ctx = document.getElementById("PKI-chart-HF-2");

    var data_chart = {
      labels : [
        'Femmes',
        'Hommes',
        'N/A'
      ],
      datasets : [
        {
          data : input_data[0],
          backgroundColor: [
            "#1d8ce0",
            "#9b59b6",
            "#bdc3c7"
          ],
          hoverBackgroundColor:  [
            "#1d8ce0",
            "#9b59b6",
            "#bdc3c7"
          ]
        }
      ]
    }

    var options_chart = {
      legend : {
        position:'right',
        display : false,
        labels : {
          boxWidth : 10,
          fontSize : 13,
          padding : 10
        }
      }
    }

    if (ctx != null) {
      var myPieChart = new Chart(ctx,{
        type: 'doughnut',
        data: data_chart,
        options : options_chart
      });
    };

    if (document.getElementsByClassName('PKI-chart-HF').length > 0) {
      new Chartist.Pie('.PKI-chart-HF', data, options);
    }

    var sum = _.reduce(input_data[2], function(memo, num){ return memo + num; }, 0);
    var percentage_pop = []

    var age_labels =  ['N/A','- 18 ans ', '18-25 ans ', '25-35ans ', '35-50 ans ', '50-65 ans', '+ 65 ans'];

    for (var i = 0; i < input_data[2].length; i++) {
      age_labels[i] += '<br/> (' + (Math.round(((input_data[2][i]/sum)*100)*100)/100).toString() + " %)";
    };

    var data = {
      labels: age_labels,
      series: [
        input_data[2]
      ]
    };

    // var ctx2 = document.getElementById("PKI-chart-age-2");

    // var data_chart_2 = {
    //   labels : age_labels,
    //   datasets : [
    //     {
    //           label : "Échantillon",
    //           backgroundColor: [
    //             '#1d8ce0',
    //             '#1d8ce0',
    //             '#1d8ce0',
    //             '#1d8ce0',
    //             '#1d8ce0',
    //             '#1d8ce0',
    //             '#1d8ce0'
    //           ],
    //           data : input_data[2]
    //     }
    //   ]
    // }
    // var option_chart_2 = {
    //   responsive:  true,
    //   maintainAspectRatio: false
    // }

    // var myBarChat = new Chart(ctx2, {
    //   type : 'bar',
    //   data : data_chart_2,
    //   options : option_chart_2
    // })


    if (document.getElementsByClassName('PKI-chart-age').length > 0) {
      new Chartist.Bar('.PKI-chart-age', data).on("draw", function(bars) {
        bars.element.animate({
          y2: {
            begin: 0,
            dur: 1000,
            from: bars.y1,
            to: bars.y2
          }
        })
      });
    }

    var modified_labels = [];

    for (var i = 0; i < input_data[1][0].length; i++) {
      modified_labels.push({meta : input_data[1][0][i].toString(), value : input_data[1][1][i]})
    };

    var data = {
      series: [modified_labels]
    };

    if (document.getElementsByClassName('PKI-chart-bdv').length > 0) {

      var chart = new Chartist.Bar('.PKI-chart-bdv', data).on("draw", function(bars) {
        bars.element.animate({
          y2: {
            begin: 0,
            dur: 1000,
            from: bars.y1,
            to: bars.y2
          }
        })
      });

      var addedEvents = false;

      chart.on('draw', function() {
        if (!addedEvents) {

          $('.ct-bar').on('mouseover', function() {
            if ($(this).attr('ct:meta') != undefined) {
              $('.ct-tooltip')[0].innerHTML = $(this).attr('ct:meta');
            }
          });

          $('.ct-bar').on('mouseout', function() {
            if ($(this).attr('ct:meta') != undefined) {
              $('.ct-tooltip')[0].innerHTML = '';
            }
          });

        }
      });
    }


}
