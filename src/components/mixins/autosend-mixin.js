// displaying the autosend variable tags requires some work, this file contains the generic functions
export const autosend = {
  data : function() {
    return {
      autosendVars : [
        "gender",
        "firstname",
        "surname",
      ],

      autosendTagType : "em",

      autosendClass : "autosend-img-tag",
    }
  },

  methods : {
    varToTag : function(varString) {
      let tagString = varString;

      // replace all $v with their corresponding tag
      for (let i = 0; i < this.autosendVars.length; i++) {
        let v = this.autosendVars[i];
        tagString = tagString.replace(new RegExp("\\$" + v, "g"), this.getTag(v));
      }

      return tagString;
    },

    tagToVar : function(tagString) {
      let varString = tagString;

      // replace all tags with their corresponding var
      for (let i = 0; i < this.autosendVars.length; i++) {
        let v = this.autosendVars[i];
        let tagRegex = this.getTagRegex(v);
        tagString = tagString.replace(new RegExp(tagRegex, "g"), "$" + v);
      }

      return tagString;
    },

    getTag : function(tag) {
      // images work much better, especially in an editor
      let path = this.$route.path;
      // TODO - test on integration, not just local
      // src should be at top level, so if path is a sub-path, must go up the structure
      // take off leading/trailing "/" for consistency
      if (path.substr(0, 1) == "/") {
        path = path.substr(1);
      }
      if (path.substr(-1,1) == "/") {
        path = path.substr(0, path.length - 1);
      }
      // count the number of remaining "/" characters; this is the number of sub paths
      let numSubs = (path.match(/\//g) || []).length;
      // add a "../" for each sub path, or a "./" of there are no sub paths
      // Array(numSubs + 1) results in an empty array with numSubs "between" spots filled with the join string, "../"
      let srcPrefix = (numSubs > 0 ? Array(numSubs + 1).join("../") : "./");
      return '<img class="' + this.autosendClass + '" src="' + srcPrefix + 'static/images/' + tag + '-var.png"/>';
    },

    getTagRegex : function(tag) {
      // quill can change the order of src and class, so have a more relaxed regex to find the tag
      // in between the <img ... >, there can be any alphabetic character, number, period, dash, slash equal sign, space
      // but importantly no ">" or "<" so that we stay in the same tag
      let fillerRegex = "[a-zA-Z0-9\\.\\\\\\/\\s\'\"=-]*";
      // note: img is a self closing tag, so ends with just ">"
      return '<img' + fillerRegex + tag + fillerRegex + '>';
    },

    // takes an optional additional class
    tagOpen : function(c) {
      let classStr = "";
      if (c != undefined) {
        classStr = " " + c;
      }
      // contenteditable='false' 
      return "<" + this.autosendTagType + " class='auto-send-preview-tag" + classStr + "' style='font-style:normal;'>";
    },
  }
}