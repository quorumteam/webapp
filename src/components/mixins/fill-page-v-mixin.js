import $ from "jquery"

export const fillPageVMixin = {
// module.exports = {
  methods : {
    /* Set the selector's height so that if fills the screen vertically, until the root html element fills the screen
     * As a consequence, this only works when the html tag does not already fill the screen.  If that is the case,
     * then you should be able to fill the screen with height: 100% or some similar css solution without using 
     * dynamic css
     *
     * Options: by default, this function will expand the first div with the ".fill-page-v" class, unless the 
     * component has a variable fillPageVMixinSelector; in that case, it will expand the first div that matches
     * that selector
     *
     * requires jQuery
     */
    fillPageV : function() {
      let selector = ".fill-page-v"
      if (this.fillPageVMixinSelector && (typeof this.fillPageVMixinSelector) == "string") {
        selector = this.fillPageVMixinSelector;
      }

      let el = $(selector);

      if (el.length <= 0) {
        // no matching element
        return;
      }

      // in the case of multiple matches, expand the first match
      el = $($(selector)[0]);
      
      let objectHeight = el.outerHeight();
      let objectTop = el.offset().top;
      let objectBottom = objectTop + objectHeight;
      
      let htmlHeight = $("html").outerHeight();
      
      let windowHeight = $(window).height();

      let height = windowHeight - objectTop - (htmlHeight - objectBottom);
      el.outerHeight(height);
    },
  },

  // set the formatting on mount
  mounted : function() {
    this.fillPageV();
  },

  // reset the formatting if anything changes
  updated : function() {
    this.fillPageV();
  },
}