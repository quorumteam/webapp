export const userIconColor = {
  data : function() {
    return {
      // Should match number of colors defined in shared-icon-colors.scss
      numColors: 9,
      // prefix (befoe number) for all icon color classes defined in user-icon-colors.scss
      classPrefix : "user-icon-color-",
    }
  },

  methods : {
    // Use Java's hash function
    // source: http://werxltd.com/wp/2010/05/13/javascript-implementation-of-javas-string-hashcode-method/
    hashString : function(str) {
      var hash = 0, i, chr;
      if (str.length === 0) return hash;
      for (i = 0; i < str.length; i++) {
        chr   = str.charCodeAt(i);
        hash  = ((hash << 5) - hash) + chr;
        hash |= 0; // Convert to 32bit integer
      }
      return hash;
    },

    getIndexFromString : function(str) {
      // need a positive number, so get the positive mod
      return ((this.hashString(str) % this.numColors) + this.numColors) % this.numColors;
    },

    getClassFromString : function(str) {
      // first hash the string using Java's hash function
      return this.classPrefix + this.getIndexFromString(str);
    },

    // single function so that we can use a consistent function
    // user should have a firstname and/or surname field
    getIndexFromUser : function(user) {
      // use the user's full name
      var str = "";
      if (user) {
        if (user.firstname) {
          str = str + user.firstname
        }
        if (user.surname) {
          str = str + user.surname
        }
      }
      return this.getIndexFromString(str);
    },

    // single function so that we can use a consistent function
    // user should have a firstname and/or surname field
    getClassFromUser : function(user) {
      // first hash the string using Java's hash function
      return this.classPrefix + this.getIndexFromUser(user);
    },
  }
}