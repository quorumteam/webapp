L.drawLocal = {

	draw: {
		toolbar: {

			actions: {
				title: 'Annuler le tracer',
				text: 'Annuler'
			},
			finish: {
				title: 'Terminer le tracer',
				text: 'Terminer'
			},
			undo: {
				title: 'Supprimer le dernier point',
				text: 'Supprimer le dernier point'
			},
			buttons: {
				polyline: 'Tracer une ligne',
				polygon: 'Tracer un polygone',
				rectangle: 'Tracer un rectangle',
				circle: 'Tracer un cercle',
				marker: 'Positioner un repère'
			}
		},
		handlers: {
			circle: {
				tooltip: {
					start: 'Click and drag to draw circle.'
				},
				radius: 'Rayon'
			},
			marker: {
				tooltip: {
					start: 'Click map to place marker.'
				}
			},
			polygon: {
				tooltip: {
					start: 'Cliquez pour commencer le tracer',
					cont: 'Cliquez pour continuer le tracer',
					end: 'Cliquez sur le premier point pour terminer'
				}
			},
			polyline: {
				error: '<strong>Error:</strong> shape edges cannot cross!',
				tooltip: {
					start: 'Click to start drawing line.',
					cont: 'Click to continue drawing line.',
					end: 'Click last point to finish line.'
				}
			},
			rectangle: {
				tooltip: {
					start: 'Click and drag to draw rectangle.'
				}
			},
			simpleshape: {
				tooltip: {
					end: 'Release mouse to finish drawing.'
				}
			}
		}
	},
	edit: {
		toolbar: {
			actions: {
				save: {
					title: 'Sauvegarder',
					text: 'Sauvegarder'
				},
				cancel: {
					title: "Annuler l'editon",
					text: 'Annuler'
				},
				clearAll:{
					title: 'Supprimer tout',
					text: 'Supprimer tout'
				}
			},
			buttons: {
				edit: 'Editer',
				editDisabled: 'Aucun tracer à éditer',
				remove: 'Supprimer',
				removeDisabled: 'Aucun tracer à supprimer'
			}
		},
		handlers: {
			edit: {
				tooltip: {
					text: 'Drag handles, or marker to edit feature.',
					subtext: 'Click cancel to undo changes.'
				}
			},
			remove: {
				tooltip: {
					text: 'Cliquez sur le tracer pour le supprimer'
				}
			}
		}
	}
};
